### The Gitlab ci and deploy to AWS Guid

## Setup Guilde
This is a sample code for a Medium article, First of all I recommend you to read that.
[CI/CD Using Gilab CI And Amazon AWS S3](https://medium.com/@mranjbar.z2993/ci-cd-using-gitlab-ci-and-amazon-aws-s3-b9e24145d2d6)

## Set up a sample react app
For feeling real condition, I create a website with create-react-app to check CI/CD for a react application.

for starting the project

* `npm install`
* `npm run build`
* `npm start`



### ps

All I added to project is   [./.gitlab-ci.yml](./.gitlab-ci.yml) , and [./aws-deploy.js](./aws-deploy.js) , So if you want
activate github CI/CD (with amazon AWS s3 deployment)
you should just add these two files to your project and
```
       "deploy-aws": "node aws-upload.js"
```

to   `package.json` `scripts`
